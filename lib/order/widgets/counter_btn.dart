import 'package:flutter/material.dart';

class CounterBtn extends StatefulWidget {
  const CounterBtn({Key? key}) : super(key: key);

  @override
  _CounterBtnState createState() => _CounterBtnState();
}

class _CounterBtnState extends State<CounterBtn> {
  int count = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 26,
      decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.black12),
          borderRadius: BorderRadius.circular(30)),
      child: Row(
        children: [
          IconButton(
              onPressed: () {
                setState(() {
                  count--;
                });
              },
              icon: Icon(
                Icons.remove,
                size: 14,
                color: Colors.black87,
              )),
          Text(
            "$count",
            style: TextStyle(
                color: Colors.black87,
                fontFamily: "FiraSans Pro",
                fontSize: 12,
                fontWeight: FontWeight.w800),
          ),
          IconButton(
              onPressed: () {
                setState(() {
                  count++;
                });
              },
              icon: Icon(
                Icons.add,
                size: 14,
                color: Colors.black87,
              )),
        ],
      ),
    );
  }
}
