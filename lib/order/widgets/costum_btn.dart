import 'package:flutter/material.dart';

class BtnCustom extends StatelessWidget {
  String? text;
  BtnCustom({
    this.text,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      margin: EdgeInsets.only(top: 10, bottom: 10, right: 24, left: 24),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        gradient: LinearGradient(
          colors: [Color(0xfff79131), Color(0xfff9a01b)],
          stops: [0, 1],
          begin: Alignment(-0.99, -0.10),
          end: Alignment(0.99, 0.10),
          // angle: 96,
          // scale: undefined,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            text!,
            style: TextStyle(
                color: Colors.white,
                fontSize: 14,
                fontWeight: FontWeight.w700,
                fontFamily: "Cairo"),
          ),
          SizedBox(
            width: 5,
          ),
          Image.asset(
            "assets/icons/next.png",
            width: 14,
            height: 14,
          )
        ],
      ),
      alignment: Alignment.center,
    );
  }
}
