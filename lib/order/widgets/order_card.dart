import 'package:flutter/material.dart';

class OrderCard extends StatelessWidget {
  Widget? child;
  Widget? btnChild;
  bool? love;

  String? name;
  String? title;
  String? cost;
  OrderCard(
      {this.child,
      this.name,
      this.title,
      this.cost,
      this.btnChild,
      this.love,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        bottom: 12,
      ),
      height: 100,
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: LinearGradient(
          colors: [
            Color(0xffDDDCE1),
            Color(0xffFCFCFC),
          ],
          stops: [0, 1],
          begin: Alignment(-0.99, -0.10),
          end: Alignment(0.99, 0.10),
          // angle: 96,
          // scale: undefined,
        ),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 2),
            blurRadius: 20,
            color: Color(0xffDDDCE1).withOpacity(0.22),
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          child!,
          // SizedBox(
          //   width: 10,
          // ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title!,
                style: TextStyle(
                  color: Colors.black45,
                  fontSize: 12,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(
                height: 6,
              ),
              Text(
                name!,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 14,
                  fontWeight: FontWeight.w800,
                ),
              ),
              SizedBox(
                height: 6,
              ),
              btnChild!,
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                cost!,
                style: TextStyle(
                  color: Color(0xffEF8042),
                  fontSize: 12,
                  fontWeight: FontWeight.w700,
                ),
              ),
              love ?? false
                  ? Image.asset(
                      "assets/icons/heart22.png",
                      width: 18,
                      height: 18,
                    )
                  : Container(),
            ],
          )
        ],
      ),
    );
  }
}
