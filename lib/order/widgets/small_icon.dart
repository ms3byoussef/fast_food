import 'package:flutter/material.dart';

class SmallIcon extends StatelessWidget {
  Color? color;
  String? icon;
  int? num;
  bool? diss;
  SmallIcon({Key? key, this.color, this.icon, this.num, this.diss})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Opacity(
          opacity: 0.3,
          child: new Container(
            margin: EdgeInsets.only(left: 10),
            decoration: new BoxDecoration(
                color: color!, borderRadius: BorderRadius.circular(50)),
          ),
        ),
        Positioned(
          child: Container(
            padding: EdgeInsets.only(
              top: 5,
            ),
            child: Image.asset(
              icon!,
              fit: BoxFit.cover,
            ),
          ),
        ),
        diss == true
            ? Positioned(
                top: 0,
                right: 2,
                child: Container(
                    width: 25,
                    height: 25,
                    decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(30)),
                    child: Center(
                      child: Text(
                        "-$num%",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: "FiraSans Pro",
                            fontSize: 12,
                            fontWeight: FontWeight.w800),
                      ),
                    )))
            : Container()
      ],
    );
  }
}
