import 'package:fast_food/order/widgets/order_card.dart';
import 'package:fast_food/order/widgets/counter_btn.dart';
import 'package:fast_food/order/widgets/costum_btn.dart';
import 'package:fast_food/order/widgets/small_icon.dart';
import 'package:flutter/material.dart';

class MyOrder extends StatefulWidget {
  const MyOrder({Key? key}) : super(key: key);

  @override
  _MyOrderState createState() => _MyOrderState();
}

class _MyOrderState extends State<MyOrder> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 25, horizontal: 24),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Row(
                      children: [
                        Image.asset(
                          "assets/icons/back1.png",
                          width: 14,
                          height: 14,
                        ),
                        SizedBox(width: 14),
                        Text(
                          "My Order ",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Image.asset(
                    "assets/icons/delete.png",
                    width: 22,
                    height: 22,
                  ),
                ],
              ),
            ),
            Container(
              height: 150,
              margin: EdgeInsets.symmetric(horizontal: 24),
              padding: EdgeInsets.only(right: 18, top: 16, left: 16),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                gradient: LinearGradient(
                  colors: [
                    Color(0xff144856),
                    Color(0xff144C59),
                  ],
                  stops: [0, 1],
                  begin: Alignment(-0.99, -0.10),
                  end: Alignment(0.99, 0.99),
                  // angle: 96,
                  // scale: undefined,
                ),
              ),
              child: Column(
                children: [
                  RowInBlueCard(
                    btnText: " Edit ",
                    icon: "assets/icons/maps-and-flags.png",
                    name: " Home ",
                    text: "625 St Mark Ave ",
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: CustomPaint(painter: DrawDottedhorizontalline()),
                  ),
                  RowInBlueCard(
                    btnText: " Choose Time ",
                    icon: "assets/icons/clock.png",
                    name: " Cook & Delivery ",
                    text: "45 Minutes",
                  )
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                children: [
                  OrderCard(
                    btnChild: CounterBtn(),
                    child: SmallIcon(
                      color: Color(0xffF2925B),
                      icon: "assets/image/burger_sandwich.png",
                      diss: true,
                      num: 7,
                    ),
                    name: "Meat Burger",
                    title: "Junk Food",
                    cost: "\$12.00",
                    love: true,
                  ),
                  OrderCard(
                    btnChild: CounterBtn(),
                    child: SmallIcon(
                      color: Color(0xffF2925B),
                      icon: "assets/image/fries_PNG12.png",
                      diss: true,
                      num: 5,
                    ),
                    name: "French Frise",
                    title: "Junk Food",
                    cost: "\$8.00",
                    love: true,
                  ),
                  OrderCard(
                    btnChild: CounterBtn(),
                    child: SmallIcon(
                      color: Color(0xffF2925B), diss: false,
                      icon: "assets/image/2688.png_300.png",
                      // num: 7,
                    ),
                    name: "Blue Drink Cup",
                    title: "Water",
                    cost: "\$8.00",
                    love: true,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 12),
              child: CustomPaint(painter: DrawDottedhorizontalline()),
            ),
            Padding(
              padding: const EdgeInsets.all(22),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Total Price",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Text(
                    "\$25.50",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
            BtnCustom(text: "Chechout"),
            SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }
}

class DrawDottedhorizontalline extends CustomPainter {
  Paint? _paint;
  DrawDottedhorizontalline() {
    _paint = Paint();
    _paint!.color = Colors.white30.withOpacity(.2); //dots color
    _paint!.strokeWidth = 1; //dots thickness
    _paint!.strokeCap = StrokeCap.square; //dots corner edges
  }

  @override
  void paint(Canvas canvas, Size size) {
    for (double i = -300; i < 300; i = i + 16) {
      // 15 is space between dots
      if (i % 3 == 0)
        canvas.drawLine(Offset(i, 0.0), Offset(i + 10, 0.0), _paint!);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class RowInBlueCard extends StatelessWidget {
  String? name;
  String? icon;
  String? text;
  String? btnText;

  RowInBlueCard({
    this.btnText,
    this.icon,
    this.name,
    this.text,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          child: Row(
            children: [
              Stack(
                children: <Widget>[
                  Opacity(
                    opacity: 0.06999999865889549,
                    child: new Container(
                        width: 40,
                        height: 40,
                        decoration: new BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15))),
                  ),
                  Positioned.fill(
                    child: Container(
                      padding: EdgeInsets.all(12),
                      child: Image.asset(icon!),
                    ),
                  ),
                ],
              ),
              SizedBox(
                width: 12,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    name!,
                    style: TextStyle(
                      color: Colors.white.withOpacity(.6),
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  Text(
                    text!,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 15),
          child: Text(
            btnText!,
            style: TextStyle(
              color: Color(0xffE57B3F).withOpacity(.6),
              fontSize: 12,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ],
    );
  }
}
