import 'package:flutter/material.dart';

class AppTheme {
  static final TextStyle smallBlackText = TextStyle(
      color: Colors.black,
      fontFamily: "Quicksand",
      fontSize: 12,
      fontWeight: FontWeight.w600);
  static final TextStyle mainHedlineB = TextStyle(
    color: Colors.black,
    fontFamily: "Quicksand",
    fontSize: 22,
    fontWeight: FontWeight.bold,
  );
  static final TextStyle costB = TextStyle(
    color: Colors.black,
    fontFamily: "Quicksand",
    fontSize: 20,
    fontWeight: FontWeight.bold,
  );
  static final TextStyle costw = TextStyle(
    color: Colors.white,
    fontFamily: "Quicksand",
    fontSize: 20,
    fontWeight: FontWeight.bold,
  );

  static final TextStyle headlineCardBlack = TextStyle(
    color: Colors.black,
    fontFamily: "Quicksand",
    fontSize: 16,
    fontWeight: FontWeight.w700,
  );
  static final TextStyle headlineCardWhite = TextStyle(
    color: Colors.white,
    fontFamily: "Quicksand",
    fontSize: 16,
    fontWeight: FontWeight.w700,
  );
  static final TextStyle grayText = TextStyle(
    color: Colors.black45,
    fontFamily: "Quicksand",
    fontSize: 14,
    fontWeight: FontWeight.w800,
  );
  static final TextStyle blackText = TextStyle(
    color: Colors.black45,
    fontFamily: "Quicksand",
    fontSize: 14,
    fontWeight: FontWeight.w800,
  );

  //fafafe
  static final TextStyle blackBigText = TextStyle(
      //last
      color: Colors.black,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.bold,
      fontFamily: 'OpenSans',
      fontSize: 20);

  static final TextStyle smallOpicityWhite = TextStyle(
    color: Colors.white.withOpacity(.65),
    fontFamily: "Quicksand",
    fontSize: 12,
    fontWeight: FontWeight.w500,
  );
  // static final Color appPrimary = Color(0xffF89499);
  static final Color mainColor = Color(0xff144C59);
  //static final Color mainColorInSalon = Color(0xFFFCC5C0); //last

  static final Color kInActiveColor = Color(0xFFfb7689);
  static final Color blackC = Colors.black87;
  static final Color kDeepPurple = Color(0xFF4E295B);
  static final Color kBabyPink = Color(0xffFFF0EB);
  static final Color kActiveColor = Colors.white;
  static final Color kListViewBackGround = Color(0xfff7f5f4);
  // static final Color secColor = Color(0xff88BAA4);

  static final Color backgroundColor = Color(0xffFFF0EB);
  // static final Color foregroundColor = Color(0xff8FDDB5);
}
