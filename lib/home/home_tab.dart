import 'package:fast_food/app_thems.dart';
import 'package:fast_food/home/widgets/category_food.dart';
import 'package:fast_food/home/widgets/main_category_food.dart';
import 'package:fast_food/home/widgets/popular_food.dart';
import 'package:flutter/material.dart';

class HomeTab extends StatelessWidget {
  const HomeTab({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(right: 24, left: 24, top: 25, bottom: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 100,
                  height: 30,
                  padding: EdgeInsets.only(
                    left: 20,
                    right: 8,
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Color(0xffF6F5F8)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Fastfood", style: AppTheme.smallBlackText),
                      SizedBox(
                        width: 4,
                      ),
                      Image.asset(
                        "assets/icons/down-arrow.png",
                        width: 14,
                        height: 14,
                      )
                    ],
                  ),
                ),
                CircleAvatar(
                  backgroundColor: Color(0xffFAC366),
                  child: Image.asset(
                    "assets/image/man.png",
                    width: 26,
                    height: 25,
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 22),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Hi lquas,",
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: "Quicksand",
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                ),
                SizedBox(height: 10),
                Text(
                  "Wellcome Back !",
                  style: AppTheme.mainHedlineB,
                ),
                SizedBox(height: 25),
                Text("Popular Food ", style: AppTheme.headlineCardBlack),
                PopularFood(),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 22, vertical: 15),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Category Food ", style: AppTheme.headlineCardBlack),
                      InkWell(
                        child: Text(
                          "See All ",
                          style: TextStyle(
                            color: Colors.black54,
                            fontFamily: "Quicksand",
                            fontSize: 14,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    MainCategoryFood(),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CategoryFood(
                            name: "Noddle",
                            rate: "4.9",
                            img: "assets/image/noddels.png",
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          CategoryFood(
                            name: "Pizza",
                            rate: "4.9",
                            img: "assets/image/pizza.png",
                          )
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
