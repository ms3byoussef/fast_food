import 'package:fast_food/app_thems.dart';
import 'package:flutter/material.dart';

class CategoryFood extends StatelessWidget {
  String? name;
  String? rate;
  String? img;
  CategoryFood({
    this.name,
    this.rate,
    this.img,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 120,
        padding: EdgeInsets.only(top: 20, left: 14),
        margin: EdgeInsets.symmetric(horizontal: 14),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: LinearGradient(
            colors: [
              Color(0xffF3F4F9),
              Color(0xffF2F3F8),
            ],
            stops: [0, 1],
            begin: Alignment(-0.0, -0.10),
            end: Alignment(0.99, 0.99),
            // angle: 96,
            // scale: undefined,
          ),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 4),
              blurRadius: 10,
              color: Color(0xFFF2F3F8).withOpacity(0.32),
            ),
          ],
        ),
        child: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(name!, style: AppTheme.blackText),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Image.asset(
                      "assets/icons/star2.png",
                      width: 14,
                      height: 14,
                    ),
                    SizedBox(width: 8),
                    Text(rate!, style: AppTheme.grayText),
                  ],
                ),
              ],
            ),
            Positioned(
              bottom: -35,
              right: -25,
              child: Container(
                // padding:
                //     EdgeInsets.only(top: 5, right: 20),
                child: Image.asset(
                  img!,
                  width: 100,
                  height: 100,
                ),
              ),
            ),
          ],
        ));
  }
}
