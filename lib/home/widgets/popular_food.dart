import 'package:fast_food/app_thems.dart';
import 'package:fast_food/details.dart';
import 'package:flutter/material.dart';

class PopularFood extends StatelessWidget {
  PopularFood({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => DetailVeiw()));
          },
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 20),
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            width: double.infinity,
            height: 200,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              gradient: LinearGradient(
                colors: [
                  AppTheme.mainColor,
                  AppTheme.mainColor,
                ],
                stops: [0, 1],
                begin: Alignment(-0.99, -0.10),
                end: Alignment(0.99, 0.10),
                // angle: 96,
                // scale: undefined,
              ),
              boxShadow: [
                BoxShadow(
                  offset: Offset(0, 4),
                  blurRadius: 10,
                  color: Color(0xFFB0CCE1).withOpacity(0.32),
                ),
              ],
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Stack(
                            children: <Widget>[
                              Opacity(
                                opacity: 0.06999999865889549,
                                child: new Container(
                                    width: 50,
                                    height: 50,
                                    decoration: new BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(15))),
                              ),
                              Positioned.fill(
                                child: Container(
                                  padding: EdgeInsets.all(12),
                                  child:
                                      Image.asset("assets/icons/hamburger.png"),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(width: 4),
                          Column(
                            children: [
                              Text("Burger", style: AppTheme.headlineCardWhite),
                              SizedBox(
                                height: 4,
                              ),
                              Text("Junk Food",
                                  style: AppTheme.smallOpicityWhite),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Stack(
                      children: <Widget>[
                        Opacity(
                          opacity: 0.06999999865889549,
                          child: new Container(
                              width: 33.4,
                              height: 33.4,
                              decoration: new BoxDecoration(
                                  color: Colors.white, shape: BoxShape.circle)),
                        ),
                        Positioned.fill(
                          child: Container(
                            padding: EdgeInsets.only(bottom: 8, top: 8),
                            child: Image.asset("assets/icons/love.png"),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 14),
                            child: Text("Meat Burger & \nCheese",
                                style: AppTheme.headlineCardWhite),
                          ),
                          Container(
                            width: 138,
                            height: 36,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                color: Color(0xffF6F5F8)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Price", style: AppTheme.grayText),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "\$12.00",
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontFamily: "Quicksand",
                                      fontSize: 19,
                                      fontWeight: FontWeight.w800),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      width: 75,
                      // margin: EdgeInsets.only(left: 25),
                      height: 100,
                      decoration: new BoxDecoration(
                          color: Color(0xff13434F),
                          borderRadius: BorderRadius.circular(50)),
                    ),
                    // Expanded(
                    //   child: Stack(
                    //     alignment: Alignment.center,
                    //     children: <Widget>[
                    //       Container(
                    //         // margin: EdgeInsets.only(left: 25),
                    //         width: 85,
                    //         height: MediaQuery.of(context).size.height * .17,
                    //         decoration: new BoxDecoration(
                    //             color: Color(0xff13434F),
                    //             borderRadius: BorderRadius.circular(90)),
                    //       ),
                    //       Positioned.fill(
                    //         child: Image.asset(
                    //           "assets/image/burger_sandwich.png",
                    //         ),
                    //       ),
                    //       // Positioned(
                    //       //     child: Container(
                    //       //   width: 35,
                    //       //   height: 35,
                    //       //   padding: EdgeInsets.only(top: 6, right: 2.5, left: 2.5),
                    //       //   decoration: BoxDecoration(
                    //       //       color: Colors.red, shape: BoxShape.circle),
                    //       //   child: Text(
                    //       //     "-7%",
                    //       //     style: TextStyle(
                    //       //         color: Colors.white,
                    //       //         fontFamily: "FiraSans Pro",
                    //       //         fontSize: 16,
                    //       //         fontWeight: FontWeight.w800),
                    //       //   ),
                    //       // ))
                    //     ],
                    //   ),
                    // ),
                  ],
                ),
              ],
            ),
          ),
        ), //
        Positioned(
          top: 90,
          right: 5,
          bottom: 70,
          child: Stack(
            children: [
              Image.asset(
                "assets/image/burger_sandwich.png",
              ),
              Positioned(
                  left: 15,
                  child: Container(
                    width: 30,
                    height: 30,
                    // padding: EdgeInsets.only(top: 6, right: 2.5, left: 2.5),
                    decoration: BoxDecoration(
                        color: Colors.red, shape: BoxShape.circle),
                    child: Center(
                      child: Text(
                        "-7%",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: "FiraSans Pro",
                            fontSize: 14,
                            fontWeight: FontWeight.w800),
                      ),
                    ),
                  ))
            ],
          ),
        ),
      ],
    );
  }
}
