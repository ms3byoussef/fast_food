import 'package:fast_food/app_thems.dart';
import 'package:flutter/material.dart';

class MainCategoryFood extends StatelessWidget {
  MainCategoryFood({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        // width: double.infinity,
        padding: EdgeInsets.only(top: 18, left: 15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: LinearGradient(
            colors: [
              Color(0xffF08040),
              Color(0xffF07F3F),
            ],
            stops: [0, 1],
            begin: Alignment(-0.99, -0.10),
            end: Alignment(0.99, 0.99),
            // angle: 96,
            // scale: undefined,
          ),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 4),
              blurRadius: 10,
              color: Color(0xFFF08040).withOpacity(0.32),
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("French Fries ", style: AppTheme.headlineCardWhite),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Text("The most delicious\n in this summer ",
                  style: AppTheme.smallOpicityWhite),
            ),
            Row(
              children: [
                Opacity(
                  opacity: .8,
                  child: Image.asset(
                    "assets/icons/star.png",
                    width: 14,
                    height: 14,
                  ),
                ),
                SizedBox(width: 8),
                Text("4.8",
                    style: AppTheme.headlineCardWhite
                        .copyWith(color: Colors.white60)),
              ],
            ),
            SizedBox(height: 11),
            Stack(
              children: <Widget>[
                new Container(
                  // margin: EdgeInsets.only(top: 10),
                  width: 130,
                  height: 130,
                  decoration: new BoxDecoration(
                      color: Color(0xffEB722F), shape: BoxShape.circle),
                ),
                Positioned(
                  bottom: -25,
                  left: -12,
                  child: Container(
                    // padding:
                    //     EdgeInsets.only(top: 5, right: 20),
                    child: Image.asset(
                      "assets/image/fries_PNG12.png",
                      width: 150,
                      height: 150,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
