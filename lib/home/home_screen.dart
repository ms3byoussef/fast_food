import 'package:dot_navigation_bar/dot_navigation_bar.dart';
import 'package:fast_food/details.dart';
import 'package:fast_food/home/home_tab.dart';
import 'package:fast_food/order/order_screen.dart';
import 'package:flutter/material.dart';

List<Widget> _SelectedTab = [
  HomeTab(),
  Container(color: Colors.red),
  MyOrder(),
  DetailVeiw(),
];

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var _selectedTab = _SelectedTab[0];

  void _handleIndexChanged(int i) {
    setState(() {
      _selectedTab = _SelectedTab[i];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: false,
      body: _selectedTab,
      bottomNavigationBar: Container(
        child: DotNavigationBar(
          // margin: EdgeInsets.symmetric(vertical: 20),
          itemPadding: EdgeInsets.symmetric(horizontal: 10),
          backgroundColor: Color(0xffF3F4F9),

          currentIndex: _SelectedTab.indexOf(_selectedTab),
          dotIndicatorColor: Color(0xffFDFDFE),
          unselectedItemColor: Colors.black,
          //  Colors.grey[300],
          // enableFloatingNavBar: false,
          onTap: _handleIndexChanged,
          items: [
            /// Home
            DotNavigationBarItem(
                icon: Icon(Icons.home), selectedColor: Color(0xff144C59)),

            /// Search
            DotNavigationBarItem(
                icon: Icon(
                  Icons.search,
                ),
                selectedColor: Color(0xff144C59)),
            DotNavigationBarItem(
                icon: Icon(Icons.shopping_bag_outlined),
                selectedColor: Color(0xff144C59)),

            /// Profile
            DotNavigationBarItem(
              icon: Icon(Icons.settings),
              selectedColor: Color(0xff144C59),
            ),
          ],
        ),
      ),
    );
  }
}
