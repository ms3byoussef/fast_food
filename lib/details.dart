import 'package:fast_food/app_thems.dart';
import 'package:fast_food/order/widgets/costum_btn.dart';
import 'package:fast_food/order/widgets/counter_btn.dart';
import 'package:fast_food/order/widgets/order_card.dart';
import 'package:fast_food/order/widgets/small_icon.dart';
import 'package:flutter/material.dart';

class DetailVeiw extends StatefulWidget {
  DetailVeiw({Key? key}) : super(key: key);

  @override
  _DetailVeiwState createState() => _DetailVeiwState();
}

class _DetailVeiwState extends State<DetailVeiw> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff134C59),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 22, vertical: 22),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Row(
                      children: [
                        Image.asset(
                          "assets/icons/left-arrow.png",
                          width: 14,
                          height: 14,
                        ),
                        SizedBox(
                          width: 18,
                        ),
                        Text("Detail", style: AppTheme.headlineCardWhite),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 15),
                    child: Image.asset(
                      "assets/icons/love.png",
                      width: 22,
                      height: 22,
                    ),
                  ),
                ],
              ),
            ),
            Center(
              child: Stack(
                children: <Widget>[
                  Opacity(
                    opacity: 0.8,
                    child: new Container(
                      margin: EdgeInsets.only(left: 20, top: 10),
                      width: 135,
                      height: 165,
                      decoration: new BoxDecoration(
                          color: Color(0xff134351),
                          borderRadius: BorderRadius.circular(50)),
                    ),
                  ),
                  Positioned(
                    child: Container(
                      padding: EdgeInsets.only(bottom: 10),
                      child: Image.asset(
                        "assets/image/burger_sandwich.png",
                        width: 180,
                        height: 180,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  Positioned(
                      top: 8,
                      left: 2,
                      child: Container(
                          width: 42,
                          height: 42,
                          padding: EdgeInsets.only(top: 8, right: 5, left: 5),
                          decoration: BoxDecoration(
                              color: Colors.red,
                              borderRadius: BorderRadius.circular(50)),
                          child: Text(
                            "-7%",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: "FiraSans Pro",
                                fontSize: 18,
                                fontWeight: FontWeight.w800),
                          ))),
                ],
              ),
            ),
            Container(
              width: double.infinity,
              height: 550,
              decoration: BoxDecoration(
                  color: Color(0xff123849),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25))),
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                    height: 100,
                    decoration: BoxDecoration(
                        color: Color(0xff123849),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 150,
                          margin: EdgeInsets.symmetric(horizontal: 18),
                          height: 35,
                          decoration: BoxDecoration(
                              color: Color(0xff1C4151),
                              borderRadius: BorderRadius.circular(30)),
                          child: Center(
                            child: Text(" Order Upcoming",
                                style: AppTheme.headlineCardWhite),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 20),
                          child: Row(
                            children: [
                              Text(" Price", style: AppTheme.smallOpicityWhite),
                              SizedBox(
                                width: 6,
                              ),
                              Text(
                                " \$12.00",
                                style: AppTheme.headlineCardWhite,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 450,
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 18),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30))),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(" Junk Food", style: AppTheme.grayText),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: Text(" Meat Burger & Cheese",
                              style: AppTheme.mainHedlineB),
                        ),
                        Text(
                          'Pure Dairy is driven by the passion of providing finest quality cheeses .',
                          style: TextStyle(
                            color: Colors.black,
                            fontFamily: "Quicksand",
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 20),
                          child: Row(
                            children: [
                              CounterBtn(),
                              SizedBox(
                                width: 18,
                              ),
                              Image.asset(
                                "assets/icons/delivery-truck.png",
                                width: 22,
                                height: 22,
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              Text("Free Shipping",
                                  style: AppTheme.smallBlackText),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child:
                              Text("Other Foods", style: AppTheme.mainHedlineB),
                        ),
                        Expanded(
                          child: OrderCard(
                            btnChild: Verified(),
                            name: "French Frise",
                            title: "Junk Food",
                            cost: "\$10.00",
                            love: false,
                            child: SmallIcon(
                              color: Color(0xffF2925B),
                              icon: "assets/image/sandwich.png",
                              diss: true,
                              num: 7,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Row(
                            children: [
                              Expanded(child: BtnCustom(text: "Order Now")),
                              Container(
                                width: 55,
                                height: 40,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        width: 1, color: Colors.black38),
                                    borderRadius: BorderRadius.circular(30)),
                                child: Icon(Icons.shopping_bag_outlined),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Verified extends StatelessWidget {
  Verified({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 27,
      decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.black12),
          borderRadius: BorderRadius.circular(30)),
      child: Row(
        children: [
          Image.asset(
            "assets/icons/check.png",
            width: 16,
            height: 16,
          ),
          SizedBox(
            width: 4,
          ),
          Text(
            "Verified Healthcare",
            style: TextStyle(
              color: Color(0xff245655),
              fontFamily: "Quicksand",
              fontSize: 10,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
